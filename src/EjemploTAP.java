
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;

public class EjemploTAP implements ActionListener {

    public static void main(String[] args) {
        EjemploTAP objEjemplo = new EjemploTAP();
        objEjemplo.run();
    }

    public void run() {
        JFrame ventana = new JFrame("Mi primer programa");
        ventana.setLayout(new BorderLayout());
        ventana.setSize(300, 200);
        ventana.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        JLabel etiqueta = new JLabel("Hola mundo!");
        JButton boton1 = new JButton("Cerrar");
        JButton boton2 = new JButton("Boton 2");
        JButton boton3 = new JButton("Boton 3");

        ventana.add(etiqueta, BorderLayout.CENTER);
        ventana.add(boton1, BorderLayout.SOUTH);
        ventana.add(boton2, BorderLayout.WEST);
        ventana.add(boton3, BorderLayout.EAST);

        /*MiControlador miCtrl = new MiControlador();

        boton1.addActionListener(miCtrl);
        boton2.addActionListener(miCtrl);
        boton3.addActionListener(miCtrl);
         */
        ventana.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                System.out.println("Clicked on: " + e.getX() + " / " + e.getY());
            }
        });
        ventana.addMouseMotionListener(new MouseAdapter() {
            @Override
            public void mouseMoved(MouseEvent e) {
                System.out.println("Moved on: " + e.getX() + " / " + e.getY());
            }
        });

        boton1.addActionListener(this);
        boton2.addActionListener(this);
        boton3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Se presiono el boton 3!!!");
            }
        });

        ventana.setVisible(true);
    }

    /*class MiControlador implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println(e.toString());
            
            if(e.getActionCommand().equals("Cerrar")){
                System.exit(0);
            }
        }
    }
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println(e.toString());

        if (e.getActionCommand().equals("Cerrar")) {
            System.exit(0);
        }
    }
}
