package sevidorgrafico;

import java.net.*;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

public class ClienteGrafico extends javax.swing.JFrame {

    private Conexion hiloChat;

    /**
     * Creates new form ClienteGrafico
     */
    public ClienteGrafico() {
        initComponents();
        botonEnviar.setEnabled(false);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        textUsuario = new javax.swing.JTextField();
        textContra = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        textMensajes = new javax.swing.JTextArea();
        textMensaje = new javax.swing.JTextField();
        botonEnviar = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        usuariosActivos = new javax.swing.JLabel();
        botonIniciarSesion = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel1.setText("Iniciar Sesión");

        textUsuario.setText("Usuario");

        textContra.setText("Contraseña");

        textMensajes.setColumns(20);
        textMensajes.setRows(5);
        jScrollPane1.setViewportView(textMensajes);

        textMensaje.setText("Mensaje");

        botonEnviar.setText("Enviar");
        botonEnviar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEnviarActionPerformed(evt);
            }
        });

        jLabel2.setText("Usuarios en linea");

        botonIniciarSesion.setText("Login");
        botonIniciarSesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonIniciarSesionActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(textContra, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(textUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(botonIniciarSesion)))
                .addGap(31, 31, 31)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(usuariosActivos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2)
                    .addComponent(botonEnviar)
                    .addComponent(textMensaje)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 388, Short.MAX_VALUE))
                .addContainerGap(25, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(textUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(textContra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(usuariosActivos, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(textMensaje, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonIniciarSesion))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(botonEnviar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonIniciarSesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonIniciarSesionActionPerformed
        // TODO add your handling code here:
        hiloChat = new Conexion(textUsuario.getText(), textContra.getText());
        botonIniciarSesion.setEnabled(false);
        hiloChat.start();

    }//GEN-LAST:event_botonIniciarSesionActionPerformed

    private void botonEnviarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEnviarActionPerformed
        // TODO add your handling code here:
        if (textMensaje.getText().equals("salir")) {
            botonEnviar.setEnabled(false);
        }
        hiloChat.enviar(textMensaje.getText());
    }//GEN-LAST:event_botonEnviarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ClienteGrafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ClienteGrafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ClienteGrafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClienteGrafico.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClienteGrafico().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonEnviar;
    private javax.swing.JButton botonIniciarSesion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField textContra;
    private javax.swing.JTextField textMensaje;
    private javax.swing.JTextArea textMensajes;
    private javax.swing.JTextField textUsuario;
    private javax.swing.JLabel usuariosActivos;
    // End of variables declaration//GEN-END:variables
    class Conexion extends Thread {

        public boolean ejecutar = true;
        BufferedReader in;
        PrintWriter out;
        Socket clientSocket;

        String usuario;
        String contra;

        public Conexion(String _usuario, String _contra) {
            this.usuario = _usuario;
            this.contra = _contra;
        }

        void enviar(String mensaje) {
            if (out != null) {
                if (mensaje.equals("salir")) {
                    cerrar();
                } else {
                    out.println(mensaje);
                    textMensajes.setText(textMensajes.getText() + mensaje + "\n");
                }
            }
        }

        void cerrar() {
            try {
                out.println("salir");
                this.ejecutar = false;
                in.close();
                out.close();
                clientSocket.close();
                botonIniciarSesion.setEnabled(true);
            } catch (IOException ex) {
                Logger.getLogger(ClienteGrafico.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        @Override
        public void run() {
            String respuesta = "";
            try {
                clientSocket = new Socket("25.50.223.0", 4444);
                out = new PrintWriter(clientSocket.getOutputStream(), true);
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out.println(usuario);//Manda el nombre del usuario
                out.println(contra); //Manda la contraseña
                respuesta = in.readLine(); //Lee ok/contraseña incorrecta
                System.out.println(respuesta);
                if (!respuesta.equals("Autenticado!")) {
                    botonIniciarSesion.setEnabled(true);
                    botonEnviar.setEnabled(false);
                    textMensajes.setText(textMensajes.getText() + "Usuario o contraseña incorrectos" + "\n");
                    cerrar();
                } else {
                    System.out.println("Logueado");
                    botonEnviar.setEnabled(true);
                }
                while (ejecutar) {
                    System.out.println("");
                    respuesta = in.readLine();
                    if (respuesta != null) {
                        if (respuesta.charAt(0) == '/') {
                            usuariosActivos.setText(respuesta.substring(1));
                        } else {
                            textMensajes.setText(textMensajes.getText() + respuesta + "\n");
                        }
                    }
                }
            } catch (IOException ex) {
                Logger.getLogger(ClienteGrafico.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
    }
}
